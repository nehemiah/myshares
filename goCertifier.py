from SharesLib import *
init()
andersList = ['J&KBANK','L&TFH','M&MFIN','M&M']
toTrend=['ABB','ACC','ADANIENT','ADANIPORTS','ADANIPOWER','ABIRLANUVO','ALBK','AMARAJABAT','AMBUJACEM','AMTEKAUTO','ANDHRABANK','APOLLOHOSP','APOLLOTYRE','ASHOKLEY','ASIANPAINT','AUROPHARMA','AXISBANK','BAJAJ-AUTO','BAJFINANCE','BAJAJFINSV','BAJAJHLDNG','BANKBARODA','BANKINDIA','BATAINDIA','BEL','BHARATFORG','BHEL','BPCL','BHARTIARTL','BHUSANSTL','BIOCON','BOSCHLTD','BRITANNIA','CESC','COREEDUTEC','CADILAHC','CAIRN','CANBK','CASTROL','CENTRALBK','CENTURYTEX','CHAMBLFERT','CIPLA','CUB','COALINDIA','COLPAL','CONCOR','CROMPGREAV','CUMMINSIND','DLF','DABUR','DENABANK','DHFL','DIVISLAB','DRREDDY','EICHERMOT','EMAMILTD','EXIDEIND','FEDERALBNK','FINANTECH','GAIL','GMRINFRA','GVKPIL','GITANJALI','GSKCONS','GLAXO','GLENMARK','GODREJCP','GODREJIND','GRASIM','GUJFLUORO','GUJRATGAS','GMDCLTD','GSPL','HCLTECH','HDFCBANK','HATHWAY','HAVELLS','HEROMOTOCO','HEXAWARE','HINDALCO','HINDPETRO','HINDUNILVR','HDFC','HDIL','ITC','ICICIBANK','IDBI','IDFC','IFCI','INGVYSYABK','IRB','IVRCLINFRA','IDEA','INDIACEM','INDIAINFO','IBREALEST','INDIANB','INDHOTEL','IOB','IGL','INDUSINDBK','INFY','IPCALAB','JSWENERGY','JSWSTEEL','JISLJALEQS','JPASSOCIAT','JPPOWER','JINDALSAW','JINDALSTEL','JUBLFOOD','KTKBANK','KARURVYSYA','KOTAKBANK','LICHSGFIN','LITL','LT','LUPIN','MRF','MADRASCEM','MARICO','MARUTI','MAX','MCLEODRUSS','MINDTREE','MOTHERSUMI','MPHASIS','NHPC','NMDC','NTPC','ONGC','OIL','OPTOCIRCUI','OFSS','ORIENTBANK','ORISSAMINE','PTC','PETRONET','PIDILITIND','PIPAVAVDOC','PEL','PFC','POWERGRID','PUNJLLOYD','PNB','RALLIS','RANBAXY','RAYMOND','RELCAPITAL','RCOM','RELIANCE','RELINFRA','RPOWER','RECLTD','SESAGOA','SHREECEM','RENUKA','SHRIRAMCIT','SRTRANSFIN','SIEMENS','SINTEX','SOBHA','SOUTHBANK','SBIN','SAIL','STAR','SUNPHARMA','SUNTV','SUZLON','SYNDIBANK','TV18BRDCST','TATACHEM','TATACOMM','TCS','TATAGLOBAL','TATAMOTORS','TATAPOWER','TATASTEEL','TECHM','TITAN','TORNTPOWER','UCOBANK','ULTRACEMCO','UNIONBANK','UNITECH','UBL','UNIPHOS','MCDOWELL-N','VIDEOIND','VIJAYABANK','VOLTAS','WELCORP','WOCKPHARMA','YESBANK','ZEEL']
date=getPreviousNSEDays(today(),20)
print date
totalSold=0
totalBought=0
for x in range(0,80):
    for y in toTrend:
        if getGo({'symbol':y,'date':date})[ANS] == 1:
            day0Price = getQuote({'symbol':y,'date':getNextNSEDays(date,1)})[ANS]
            day1Price = getQuote({'symbol':y,'date':getNextNSEDays(date,2)})[ANS]
            day2Price = getQuote({'symbol':y,'date':getNextNSEDays(date,3)})[ANS]
            day3Price = getQuote({'symbol':y,'date':getNextNSEDays(date,4)})[ANS]
            day4Price = getQuote({'symbol':y,'date':getNextNSEDays(date,5)})[ANS]
            day5Price = getQuote({'symbol':y,'date':getNextNSEDays(date,6)})[ANS]
            prices = [day0Price,day1Price,day2Price,day3Price,day4Price,day5Price]
            res=executeBTST({'prices':prices})
            totalSold += res[SOLD]
            totalBought += res[BOUGHT]
            if getChangePercentage({'old':res[BOUGHT],'new':res[SOLD]})[ANS] > 5:
                print "Study "+y+" as on"+date 
    date=getPreviousNSEDay(date)
print getChangePercentage({'old':totalBought,'new':totalSold})[ANS]
print "Bought at "+`totalBought`+" and Sold at "+`totalSold`
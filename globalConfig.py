ANS ="answer"
SUCCESS="success"
FAILED="failed"
STATUS = "status"
MESSAGE = "message"
CONTENT = "content"
URL = 'url'
REQ = 'req'
EXCEPTION = 'exception'
METHOD= 'method'

import MySQLdb
import MySQLdb.cursors
import urllib2
import re
import datetime
import inspect

DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASSWORD = 'root'
DB_NAME = 'iniyaval_iniya'
conn = MySQLdb.connect(host= DB_HOST,user=DB_USER,passwd=DB_PASSWORD,db=DB_NAME)
cursor = conn.cursor (MySQLdb.cursors.DictCursor)

TEXT_HEADER="Content-type: text/html\n\n"
JSON_HEADER="Content-type: application/json\n\n"

import logging
LOGFILENAME = 'log.log'

CRITICAL=logging.CRITICAL
ERROR	=logging.ERROR
WARNING	=logging.WARNING
INFO	=logging.INFO
DEBUG	=logging.DEBUG
NOTSET	=logging.NOTSET

LOGLEVEL=ERROR
logging.basicConfig(filename=LOGFILENAME,level=LOGLEVEL)

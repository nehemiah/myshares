from SharesLib import *
init()
toTrend=['NIFTY','BANKNIFTY','CNXIT','ABB','ACC','ADANIENT','ADANIPORTS','ADANIPOWER','ABIRLANUVO','ALBK','AMARAJABAT','AMBUJACEM','AMTEKAUTO','ANDHRABANK','APOLLOHOSP','APOLLOTYRE','ASHOKLEY','ASIANPAINT','AUROPHARMA','AXISBANK','BAJAJ-AUTO','BAJFINANCE','BAJAJFINSV','BAJAJHLDNG','BANKBARODA','BANKINDIA','BATAINDIA','BEL','BHARATFORG','BHEL','BPCL','BHARTIARTL','BHUSANSTL','BIOCON','BOSCHLTD','BRITANNIA','CESC','COREEDUTEC','CADILAHC','CAIRN','CANBK','CASTROL','CENTRALBK','CENTURYTEX','CHAMBLFERT','CIPLA','CUB','COALINDIA','COLPAL','CONCOR','CROMPGREAV','CUMMINSIND','DLF','DABUR','DENABANK','DHFL','DIVISLAB','DRREDDY','EICHERMOT','EMAMILTD','EXIDEIND','FEDERALBNK','FINANTECH','GAIL','GMRINFRA','GVKPIL','GITANJALI','GSKCONS','GLAXO','GLENMARK','GODREJCP','GODREJIND','GRASIM','GUJFLUORO','GUJRATGAS','GMDCLTD','GSPL','HCLTECH','HDFCBANK','HATHWAY','HAVELLS','HEROMOTOCO','HEXAWARE','HINDALCO','HINDPETRO','HINDUNILVR','HDFC','HDIL','ITC','ICICIBANK','IDBI','IDFC','IFCI','INGVYSYABK','IRB','IVRCLINFRA','IDEA','INDIACEM','INDIAINFO','IBREALEST','INDIANB','INDHOTEL','IOB','IGL','INDUSINDBK','INFY','IPCALAB','JSWENERGY','JSWSTEEL','JISLJALEQS','JPASSOCIAT','JPPOWER','J&KBANK','JINDALSAW','JINDALSTEL','JUBLFOOD','KTKBANK','KARURVYSYA','KOTAKBANK','L&TFH','LICHSGFIN','LITL','LT','LUPIN','MRF','MADRASCEM','M&MFIN','M&M','MARICO','MARUTI','MAX','MCLEODRUSS','MINDTREE','MOTHERSUMI','MPHASIS','NHPC','NMDC','NTPC','ONGC','OIL','OPTOCIRCUI','OFSS','ORIENTBANK','ORISSAMINE','PTC','PETRONET','PIDILITIND','PIPAVAVDOC','PEL','PFC','POWERGRID','PUNJLLOYD','PNB','RALLIS','RANBAXY','RAYMOND','RELCAPITAL','RCOM','RELIANCE','RELINFRA','RPOWER','RECLTD','SESAGOA','SHREECEM','RENUKA','SHRIRAMCIT','SRTRANSFIN','SIEMENS','SINTEX','SOBHA','SOUTHBANK','SBIN','SAIL','STAR','SUNPHARMA','SUNTV','SUZLON','SYNDIBANK','TV18BRDCST','TATACHEM','TATACOMM','TCS','TATAGLOBAL','TATAMOTORS','TATAPOWER','TATASTEEL','TECHM','TITAN','TORNTPOWER','UCOBANK','ULTRACEMCO','UNIONBANK','UNITECH','UBL','UNIPHOS','MCDOWELL-N','VIDEOIND','VIJAYABANK','VOLTAS','WELCORP','WOCKPHARMA','YESBANK','ZEEL']
DATE=today()
Heading1=DATE
Heading2=DATE

goList = []
	
for x in toTrend:
	goNoGo = ""
	forecast = getForecast({'symbol':x,'date':getNextBusinessDay(DATE)})[ANS]
	trend = getTrend({'symbol':x,'date':DATE})[ANS]
	diff = getChangePercentage({'new':forecast, 'old':trend})[ANS]
	if diff > 10 and forecast > 10:
		goList.append(x)

date=DATE
for x in goList:
	forecast = getForecast({'symbol':x,'date':getNextBusinessDay(DATE)})[ANS]
	Heading1+="|"+x+"||||"
	Heading2+="|Quote|1 Per|3 Per|5 Per|"+`forecast`
print Heading1
print Heading2

date=DATE
for x in range(0,100):
	row=date
	for y in goList:
		quote=`getQuote({'symbol':y,'date':date})[ANS]`
		oneDayChange=`get1DayChange({'symbol':y,'date':date})[ANS]`
		threeDayChange=`get3DayChange({'symbol':y,'date':date})[ANS]`
		fiveDayChange=`get5DayChange({'symbol':y,'date':date})[ANS]`
		trend=`getTrend({'symbol':y,'date':date})[ANS]`
		row+="|"+quote+"|"+oneDayChange+"|"+threeDayChange+"|"+fiveDayChange+"|"+trend
	print row
	date=getPreviousBusinessDay(date)
	
if isToday(DATE) and isMarketHour("NSE"):
	flush(today())
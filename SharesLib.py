from globalConfig import *
#Tables names as python constants
NSETABLE="NSEQuote"
NSETRENDTABLE="NSETrend"
NSE1DAYTABLE="NSE1DayChange"
NSE3DAYTABLE="NSE3DayChange"
NSE5DAYTABLE="NSE5DayChange"
NSEFORECASTTABLE="NSETrendForecast"
NSEGOTABLE="NSEGo"

ADDCOLUMNDATATYPE="FLOAT"
ADDCOLUMNAFTERTHIS="Date"
ADDCOLUMNREMOVALLIST=['Date']

ANS ="2answer"
SUCCESS="success"
FAILED="failed"
STATUS = "1status"
MESSAGE = "3message"
CONTENT = "content"
URL = '3url'
QUERYSTRING = '3queryString'
REQ = 'req'
EXCEPTION = 'exception'
METHOD= '0method'
UNKNOWN='unknown'
SUBRES = 'zsubres'
BOUGHT = 'bought'
SOLD = 'sold'

import MySQLdb
import MySQLdb.cursors
import urllib2
import re
import datetime
import inspect
import pprint
import jsonpickle
import json
	
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASSWORD = 'root'
DB_NAME = 'iniyaval_iniya'
conn = MySQLdb.connect(host= DB_HOST,user=DB_USER,passwd=DB_PASSWORD,db=DB_NAME)
cursor = conn.cursor (MySQLdb.cursors.DictCursor)

TEXT_HEADER="Content-type: text/html\n\n"
JSON_HEADER="Content-type: application/json\n\n"

NSEHolidays = ["2011-01-26","2011-03-02","2011-04-12","2011-04-14","2011-04-22","2011-08-15","2011-08-31","2011-09-01","2011-10-06","2011-10-26","2011-10-27","2011-11-07","2011-11-10","2011-12-06","2012-01-26","2012-02-20","2012-03-08","2012-04-05","2012-04-06","2012-05-01","2012-08-15","2012-08-20","2012-09-19","2012-10-02","2012-10-24","2012-11-14","2012-11-28","2012-12-25","2013-03-27","2013-03-29","2013-04-19","2013-04-24","2013-05-01","2013-08-09","2013-08-15","2013-09-09","2013-10-02","2013-10-16","2013-11-04","2013-11-14","2013-12-25"]

def getChangePercentage(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	new = req['new']
	old = req['old']
	if old == 0.0:
		res[STATUS]=FAILED
		res[MESSAGE]="Hmm, I usually am not interested to divide by zero :("
		return res
	res[ANS] = float(new) / float(old) * 100 - 100
	res[STATUS] = SUCCESS
	return res
	
def calculateNDayChange(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	N = req['N']
	new = getQuote({'symbol':symbol,'date':date})[ANS]
	old = getQuote({'symbol':symbol,'date':getPreviousNSEDays(date,N)})[ANS]
	res[SUBRES] = getChangePercentage({'new':new,'old':old})
	res[ANS]=res[SUBRES][ANS]
	res[STATUS]=res[SUBRES][STATUS]
	return res

def doesExist(req):
	#Checks if the give values is existing in the given column for the given table
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN}
	tableName = req['tableName']
	columnName = req['columnName']
	value = req['value']
	queryString = "select count(1) from `"+tableName+"` where `"+columnName+"`='"+value+"'"
	conn.query(queryString)
	count=conn.use_result().fetch_row()[0][0]
	if count > 0 :
		res[ANS]= True
		res[STATUS]=SUCCESS
		return res
	res[ANS]= False
	res[STATUS]=SUCCESS
	return res

def isToday(dateString):
	if datetime.datetime.today().strftime('%Y-%m-%d') == dateString:
		return True
	return False

def log(level, object):
	try:
		jsonObject = jsonpickle.encode(object)
		parsedJsonObject = json.loads(jsonObject)
		string = json.dumps(parsedJsonObject, indent=4, sort_keys=True)
	except:
		string = object
	if level == CRITICAL:
		logging.critical(string)
	if level == ERROR:
		logging.error(string)
	if level == WARNING:
		logging.warn(string)
	if level == INFO:
		logging.info(string)
	if level == DEBUG:
		logging.debug(string)
	
def addColumn(req):
#Adds a column data for a given columnName and given dataType in the alphabetical position of colums
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN}
	tableName = req['tableName']
	columnName = req['columnName']
	dataType = req['dataType']
	
	try:
		columnList = getColumnsList({'tableName':tableName})[ANS]
		afterThis = columnList[-1]
		
		for x in reversed(columnList):
			if x < columnName:
				afterThis = x
				break
		
		queryString="ALTER TABLE  `"+tableName+"` ADD  `"+columnName+"` "+dataType+" NOT NULL AFTER  `"+afterThis+"`"
		cursor.execute(queryString)
		conn.commit()
		res[STATUS]=SUCCESS
	except Exception as e:
		res[STATUS]=FAILED
		res[MESSAGE]=e
		log(ERROR, res)
	return res

def getColumnsList(req):
#Returns the list of colums of a given table in the current database
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN}
	tableName = req['tableName']
	queryString="SELECT column_name FROM information_schema.columns WHERE table_name='"+tableName+"'"
	cursor.execute(queryString)
	numrows = cursor.rowcount
	columnList = []
	for x in range(0,numrows):
		columnList.append(cursor.fetchone()['column_name'])
	res[ANS] = columnList
	res[STATUS]=SUCCESS
	return res

def findBetween(req):
#Usage req = {'string':'stringtoparse', 'start':'start string', 'end':'end string'}
	res = {}
	string = req['string']
	start = req['start']
	end = req['end']
	try:
		start1= string.index( start ) + len( start )
		end1 = string.index( end, start1 )
		res[ANS] = string[start1:end1]
	except ValueError:
		res[STATUS] = FAILED
		res[ANS] = ""
	return res

def mean(list):
	return float(sum(list))/len(list)

def today():
	return datetime.datetime.today().strftime('%Y-%m-%d')

def isMarketHour(exchangeName):
	#marketOpening = datetime.time(9, 15, 0)
	marketClosing = datetime.time(17, 00, 0)
	now = datetime.datetime.now().time()
	if now <= marketClosing:
		return True
	return False
	
def cleanFlush(date):
	queryString = "delete from `"+NSETABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSETRENDTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE1DAYTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE3DAYTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE5DAYTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSEFORECASTTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSEGOTABLE+"` where date>='"+date+"'"
	cursor.execute(queryString)
	conn.commit()
	
def flush(date):
	queryString = "delete from `"+NSETABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSETRENDTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE1DAYTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE3DAYTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSE5DAYTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSEFORECASTTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	queryString = "delete from `"+NSEGOTABLE+"` where date='"+date+"'"
	cursor.execute(queryString)
	conn.commit()
	
def pythonDateTransform(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:''}
	dateString = req['dateString']
	existingForm = req['existingForm']
	requiredForm = req['requiredForm']
	try:
		Date = datetime.datetime.strptime(dateString, existingForm)
		res[ANS]=Date.strftime(requiredForm)
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		log(ERROR, res)
	return res

def getPreviousNSEDay(date):
	date = getPreviousBusinessDay(date)
	if date in NSEHolidays:
		date = getPreviousBusinessDay(date)
	if date in NSEHolidays:
		date = getPreviousBusinessDay(date)
	return date
	
def getNextNSEDay(date):
	date = getNextBusinessDay(date)
	if date in NSEHolidays:
		date = getNextBusinessDay(date)
	if date in NSEHolidays:
		date = getNextBusinessDay(date)
	return date

def getPreviousNSEDays(date,num):
	try:
		for i in range (0, num):
			date = getPreviousNSEDay(date)
		return date
	except Exception as e:
		return ''

def getNextNSEDays(date,num):
	try:
		for i in range (0, num):
			date = getNextNSEDay(date)
		return date
	except Exception as e:
		return ''


def getPreviousBusinessDays(date,num):
	try:
		for i in range (0, num):
			date = getPreviousBusinessDay(date)
		return date
	except Exception as e:
		return ''

def getNextBusinessDay(fromDate):
	nextBuinessDate = datetime.datetime.strptime(fromDate, "%Y-%m-%d")
	nextBuinessDate = nextBuinessDate + datetime.timedelta(days=1)
	if datetime.date.weekday(nextBuinessDate) not in range(0,5):
		nextBuinessDate = nextBuinessDate + datetime.timedelta(days=1)
	if datetime.date.weekday(nextBuinessDate) not in range(0,5):
		nextBuinessDate = nextBuinessDate + datetime.timedelta(days=1)
	return nextBuinessDate.strftime('%Y-%m-%d')

def getPreviousBusinessDay(fromDate):        
	previousBuinessDate = datetime.datetime.strptime(fromDate, "%Y-%m-%d")
	previousBuinessDate = previousBuinessDate + datetime.timedelta(days=-1)
	if datetime.date.weekday(previousBuinessDate) not in range(0,5):
			previousBuinessDate = previousBuinessDate + datetime.timedelta(days=-1)
	if datetime.date.weekday(previousBuinessDate) not in range(0,5):
			previousBuinessDate = previousBuinessDate + datetime.timedelta(days=-1)
	return previousBuinessDate.strftime('%Y-%m-%d')

def getCurrentBusinessDay(dateString):
	if dateString == "":
		dateString = today()
	if isWeekEnd(dateString):
		return getPreviousBusinessDay(dateString)
	return dateString
	
def getCurrentNSEDay(dateString):
	if dateString == "":
		dateString = today()
	if isWeekEnd(dateString) or (dateString in NSEHolidays):
		return getPreviousNSEDay(dateString)
	return dateString
	
def isWeekEnd(dateString):
#Tells a given date is weekend or not, Date format is "yyyy-mm-dd"
	Date = datetime.datetime.strptime(dateString, "%Y-%m-%d")
	if datetime.date.weekday(Date) not in range(0,5):
		return True
	return False

def getNSEQuote(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	toDate = pythonDateTransform({'dateString':date,'existingForm':"%Y-%m-%d", 'requiredForm':"%d/%m/%Y"})[ANS]
	fromDate = pythonDateTransform({'dateString':getPreviousNSEDays(date,2),'existingForm':"%Y-%m-%d", 'requiredForm':"%d/%m/%Y"})[ANS]
	hdr = {'User-Agent':'Mozilla/5.0','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',}
	url = "http://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/getHistoricalData.jsp?symbol="+symbol+"&fromDate="+fromDate+"&toDate="+toDate			
	try:
		req = urllib2.Request(url, headers=hdr)
		text = urllib2.urlopen(req).read ()		
		text = text.replace('\n',"")
		if text == "No Record Found":
			res[MESSAGE]=text
			res[STATUS]=FAILED
			res[URL]=url
			#log(WARNING, res)
			return res
		text = text.replace(' ',"")
		text = text.replace(',',"")
		text = text.replace('</td><td>',",")
		text = text.replace('</tbody></table>',"")
		text = text.replace('<td>',"")
		text = text.replace('</td>',"")
		text = text.replace('<tr>',"")
		text = text.replace('</tr>',"")
		text = text.replace('<table><tbody><th>Date</th><th>OpenPrice</th><th>HighPrice</th><th>LowPrice</th><th>LastTradedPrice</th><th>ClosePrice</th><th>TotalTradedQuantity</th><th>Turnover(inLakhs)</th>',"")
		text = text.split(',')[5]
		res[ANS]= float(text)
		res[STATUS]=SUCCESS
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		res[MESSAGE]=text
		res[URL]=url
		log(ERROR, res)
	return res

def getYahooQuote(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	
	yahooNotSupported=['M&M','CNXPHARMA','CNXENERGY','CNXFMCG','CNXINFRA','CNXMNC','CNXREALTY','CNXPSUBANK','CNXSERVICE','ASIANPAINT','APOLLOHOSP','BAJAJ-AUTO','BHARATFORG',
	'CUMMINSIND','FEDERALBNK','HINDUNILVR','INDUSINDBK','JINDALSTEL','MCDOWELL-N','TATAGLOBAL']
	
	if symbol in yahooNotSupported:
		res[STATUS]=FAILED
		res[MESSAGE]="Unfortunately cruel Yahoo is not supporting this stock :( ->"+symbol
		log(WARNING, res)
		return res
		
	date=getCurrentNSEDay(date)
	
	yahooDict={'L&TFH':'L&TFH','MADRASCEM':'MADRASCEM','M&MFIN':'M&MFIN','J&KBANK':'J&KBANK','NIFTY':'^NSEI', 'BANKNIFTY':'^NSEBANK', 'CNXIT':'^CNXIT', 'CNXPHARMA':'^CNXPHARMA', 'CNXENERGY':'^CNXENERGY', 'CNXFMCG':'^CNXFMCG', 'CNXINFRA':'^CNXINFRA', 'CNXMNC':'^CNXMNC', 
	'CNXREALTY':'^CNXREALTY', 'CNXPSUBANK':'^CNXPSUBANK', 'CNXSERVICE':'^CNXSERVICE', 'ACC':'ACC', 'ADANIENT':'ADANIENT', 'ADANIPORTS':'ADANIPORTSL', 'ABIRLANUVO':'ABIRLANUV', 'AMBUJACEM':'AMBUJACEM', 
	'APOLLOHOSP':'APOLLOHOSP-EQ', 'ASHOKLEY':'ASHOKLEY', 'ASIANPAINT':'ASIANPAINT', 'AXISBANK':'AXISBANK', 'BAJAJ-AUTO':'BAJAJ-AUTO', 'BAJAJFINSV':'BAJAJFINS', 'BAJAJHLDNG':'BAJAJHLDN', 
	'BANKBARODA':'BANKBAROD', 'BANKINDIA':'BANKINDIA', 'BHARATFORG':'BHARATFORG', 'BHEL':'BHEL', 'BPCL':'BPCL', 'BHARTIARTL':'BHARTIART', 'BOSCHLTD':'BOSCHLTD', 'CAIRN':'CAIRN', 
	'CANBK':'CANBK', 'CIPLA':'CIPLA', 'COALINDIA':'COALINDIA', 'COLPAL':'COLPAL', 'CONCOR':'CONCOR', 'CROMPGREAV':'CROMPGREA', 'CUMMINSIND':'CUMMINSIND', 'DLF':'DLF', 'DABUR':'DABUR', 
	'DIVISLAB':'DIVISLAB', 'DRREDDY':'DRREDDY', 'EXIDEIND':'EXIDEIND', 'FEDERALBNK':'FEDERALBN', 'GAIL':'GAIL', 'GSKCONS':'GSKCONS', 'GLAXO':'GLAXO', 'GLENMARK':'GLENMARK', 'GODREJCP':'GODREJCP', 
	'GRASIM':'GRASIM', 'HCLTECH':'HCLTECH', 'HDFCBANK':'HDFCBANK', 'HEROMOTOCO':'HEROMOTOC', 'HINDALCO':'HINDALCO', 'HINDPETRO':'HINDPETRO', 'HINDUNILVR':'HINDUNILVR', 'HDFC':'HDFC', 
	'ITC':'ITC', 'ICICIBANK':'ICICIBANK', 'IDBI':'IDBI', 'IDFC':'IDFC', 'IDEA':'IDEA', 'INDHOTEL':'INDHOTEL', 'INDUSINDBK':'INDUSINDBK', 'INFY':'INFY', 'JSWSTEEL':'JSWSTEEL', 'JPASSOCIAT':'JPASSOCIA', 
	'JINDALSTEL':'JINDALSTEL', 'KOTAKBANK':'KOTAKBANK', 'LICHSGFIN':'LICHSGFIN', 'LT':'LT', 'LUPIN':'LUPIN', 'M&M':'M&M', 'MARUTI':'MARUTI', 'MPHASIS':'MPHASIS', 'NMDC':'NMDC', 'NTPC':'NTPC', 
	'ONGC':'ONGC', 'OFSS':'OFSS', 'PETRONET':'PETRONET', 'PFC':'PFC', 'POWERGRID':'POWERGRID', 'PNB':'PNB', 'RANBAXY':'RANBAXY', 'RELCAPITAL':'RELCAPITA', 'RCOM':'RCOM', 'RELIANCE':'RELIANCE', 
	'RELINFRA':'RELINFRA', 'RPOWER':'RPOWER', 'RECLTD':'RECLTD', 'SESAGOA':'SESAGOA', 'SRTRANSFIN':'SRTRANSFIN', 'SIEMENS':'SIEMENS', 'SBIN':'SBIN', 'SAIL':'SAIL', 'SUNPHARMA':'SUNPHARMA', 
	'TATACHEM':'TATACHEM', 'TCS':'TCS', 'TATAGLOBAL':'TATAGLOBAL', 'TATAMOTORS':'TATAMOTOR', 'TATAPOWER':'TATAPOWER', 'TATASTEEL':'TATASTEEL', 'TECHM':'TECHM', 'TITAN':'TITAN', 
	'ULTRACEMCO':'ULTRACEMC', 'UNIONBANK':'UNIONBANK', 'UBL':'UBL', 'UNIPHOS':'UNIPHOS', 'MCDOWELL-N':'MCDOWELL-N', 'YESBANK':'YESBANK', 'ZEEL':'ZEEL'}
	
	try:
		symbol=yahooDict[symbol]
	except Exception as e:
		#res[EXCEPTION]=e
		res[MESSAGE]="Yahoo dictionary is imcomplete, Add ASAP"
		res[STATUS]=FAILED
		log(ERROR, res)
		return res

	year=date.split('-')[0]
	month=int(date.split('-')[1])-1
	day=date.split('-')[2]
	
	if '^' in symbol:
		symbol=symbol.replace('^',"%5E")
	else:
		symbol=symbol+".NS"
	
	symbol=symbol.replace('&',"%26")
	
	url = "http://ichart.finance.yahoo.com/table.csv?s="+symbol+"&a="+`month`+"&b="+day+"&c="+year+"&d="+`month`+"&e="+day+"&f="+year

	try:
		response = urllib2.urlopen(url)
		text = response.read()
		res[ANS] =  float(text.split(',')[-1])
		res[STATUS]=SUCCESS
		return res
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		res[URL]=url
		log(WARNING, res)
		return res

def getGoogleQuote(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	
	symbol=symbol.replace('&',"%26")
	
	#If it is today go for a rquote by google, Make sure you delete the row once the report is generated
	if isToday(date):
		res[SUBRES] = getGoogleRealTimeQuote({'symbol':symbol})
		res[ANS] = res[SUBRES][ANS]
		res[STATUS]=res[SUBRES][STATUS]
		return res
	else:
		try:
			url="http://www.google.com/finance/historical?q=NSE%3A"+symbol+"&enddate="+date+"&num=1"
			text = urllib2.urlopen(url).read()
			text = text.replace(',',"")
			scrapedDate=findBetween({'string':text,'start':"<td class=\"lm\">",'end':"<td class=\"rgt\">"})[ANS].replace('\n',"")
			scrapedDate=pythonDateTransform({'dateString':scrapedDate, 'existingForm':"%b %d %Y", 'requiredForm':"%Y-%m-%d"})[ANS]
			if scrapedDate == date:
				elem = re.findall(r'">[0-9]*[.][0-9]*',text)[-1]
				quote = re.findall(r'[0-9]*[.][0-9]*',elem)[-1]
				res[ANS] = float(quote)
				res[STATUS]=SUCCESS
				return res
			else:
				res[STATUS]=FAILED
				res[URL]=url
				res[MESSAGE]="Seems required date range is not found :("
				log(WARNING, res)			
		except Exception as e:
			#res[EXCEPTION]=e
			res[STATUS]=FAILED
			res[URL]=url
			log(ERROR, res)
	return res
	
def getGoogleRealTimeQuote(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	symbol=symbol.replace('&',"%26")
	try:
		url="http://finance.google.com/finance/info?q=NSE%3A"+symbol
		text = urllib2.urlopen(url).read()
		text = text.replace(',',"")
		elem = re.findall(r'Rs[.][0-9]*[.][0-9]*',text)[-1]
		quote = elem.replace("Rs.","")
		res[ANS]=float(quote)
		res[STATUS]=SUCCESS
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		res[URL]=url
		log(ERROR, res)
	return res
	
def fetchQuoteFromInternet(req):
	#Fetch historical quote from nseindia.com for the given scrip and given date
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	if isToday(date):
		res[SUBRES] = getGoogleRealTimeQuote({'symbol':symbol})
	else:
		res[SUBRES] = getNSEQuote({'symbol':symbol, 'date':date})
	
	res[STATUS] = res[SUBRES][STATUS]
	#If NSE quote is not available or throwing exception then go for Google quote service
	if res[STATUS] == FAILED:
		res[SUBRES] = getGoogleQuote({'symbol':symbol, 'date':date})
		res[STATUS] = res[SUBRES][STATUS]
	
	#Sometimes even the intelligent Google is dump, so at that time alone it is not harm to use Yahoo
	if res[STATUS] == FAILED:
		res[SUBRES] = getYahooQuote({'symbol':symbol, 'date':date})
		res[STATUS] = res[SUBRES][STATUS]
	
	res[ANS] = res[SUBRES][ANS]
	return res
	
def calculateTrend(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	percentageChangeList=[]
	date=getNextNSEDay(date)
	oldQRes = None
	for x in range(0,21):
		date=getPreviousNSEDay(date)
		if x==0:
			oldQRes=getQuote({'symbol':symbol,'date':date})
			oldQ = oldQRes[ANS]
			continue
		newQ=oldQ
		if newQ == 0.0 or oldQ == 0.0:
			res[STATUS] = FAILED
			res[MESSAGE] =	"Atleast one quote found to be 0.0 while calculating trend :("
			res[SUBRES] = oldQRes
			log(WARNING, res)
			return res
		oldQRes=getQuote({'symbol':symbol,'date':date})
		oldQ = oldQRes[ANS]
		if oldQ == 0:
			percentageChange=0.0
		else:
			percentageChange=newQ / oldQ * 100 - 100
			percentageChangeList.append(percentageChange)
	fiveAverage=mean(percentageChangeList[0:5])
	tenAverage=mean(percentageChangeList[0:10])
	fifteenAverage=mean(percentageChangeList[0:15])
	twentyAverage=mean(percentageChangeList)
	res[ANS] = twentyAverage + fifteenAverage * 2 + tenAverage * 3 + fiveAverage * 4
	res[STATUS] = SUCCESS
	return res
	
def forecast(req):
#This forecast method uses linear regression method for the request x based on the knowXs and knownYs
#It is based on fitting a line using y=mx+b formula where m(slope) and b is calculated by the below formulae, 
#Thanks to python for such faster implementation :)
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	x		= req['x']
	knownXs = req['knownXs']
	knownYs = req['knownYs']
	if len(knownXs) != len(knownYs):
		res[STATUS]= FAILED
		res[MESSAGE] = "Dude the size of knownX and knownY are not matching ! Check the request once :)"
		log(WARNING, res)
		return res
	xBar = mean(knownXs)
	yBar = mean(knownYs)
	list_x_minus_xbar_into_y_minus_ybar=[]
	list_x_minus_xbar_whole_squared=[]
	for i in range(0,len(knownXs)):
		x_minus_xbar_into_y_minus_ybar=(knownXs[i]-xBar)*(knownYs[i]-yBar)
		x_minus_xbar_whole_squared=(knownXs[i]-xBar)*(knownXs[i]-xBar)
		list_x_minus_xbar_into_y_minus_ybar.append(x_minus_xbar_into_y_minus_ybar)
		list_x_minus_xbar_whole_squared.append(x_minus_xbar_whole_squared)
	m = sum(list_x_minus_xbar_into_y_minus_ybar)/sum(list_x_minus_xbar_whole_squared)
	b = yBar-m*xBar
	res[ANS] = m*x+b
	res[STATUS] = SUCCESS
	return res

def myForecast(req):
#Since I don't have complete satisfication of linear method so I wanted to make it slightly exponential-
#using weighted average of 3dayForecast, 4dayForecast and 5dayForecast
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	x		= req['x']
	knownXs = req['knownXs']
	knownYs = req['knownYs']
	threeDayForecast=forecast({'x':x, 'knownXs':knownXs[0:3], 'knownYs':knownYs[0:3]})[ANS]
	fourDayForecast=forecast({'x':x, 'knownXs':knownXs[0:4], 'knownYs':knownYs[0:4]})[ANS]
	fiveDayForecast=forecast({'x':x, 'knownXs':knownXs[0:5], 'knownYs':knownYs[0:5]})[ANS]
	res[ANS] = (3*threeDayForecast+2*fourDayForecast+fiveDayForecast)/6
	res[STATUS] = SUCCESS
	return res

def calculateGoNoGo(req):
    res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:2}
    symbol = req['symbol']
    date = req['date']
    forecast = getForecast({'symbol':symbol,'date':getNextNSEDay(date)})[ANS]
    trend = getTrend({'symbol':symbol,'date':date})[ANS]
    trend2 = getTrend({'symbol':symbol,'date':getPreviousNSEDay(date)})[ANS]
    diff = getChangePercentage({'new':forecast, 'old':trend})[ANS]
    diff2 = getChangePercentage({'new':trend, 'old':trend2})[ANS]
    if forecast > 10 and diff > 25 and trend > 0 and diff2 > 25:
        res[ANS] = 1
    res[STATUS] = SUCCESS
    return res

def calculateTrendForecast(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	x = 0;
	knownXs = [1, 2, 3, 4, 5]
	knownYs = []
	for i in range(1,6):
		res[SUBRES] = getTrend({'symbol':symbol,'date':getPreviousNSEDays(date, i)})
		if res[SUBRES][STATUS] == FAILED:
			res[STATUS] = FAILED
			res[MESSAGE] = "At least one trend was not success, So forcast failed"
			log(WARNING, res)
			return res
		knownYs.append(res[SUBRES][ANS])
	res[ANS] = myForecast({'x':x, 'knownXs':knownXs, 'knownYs':knownYs})[ANS]
	res[STATUS] = SUCCESS
	return res
	
def setValueFromSymbolDateTable(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	tableName = req['tableName']
	symbol = symbol.upper()
	columnList = getColumnsList({'tableName':tableName})[ANS]
	value = 0.0

	#If the requested scrip is not in the table add it as a column
	if symbol not in columnList :
	        addColumn({'tableName':tableName, 'columnName':symbol, 'dataType':ADDCOLUMNDATATYPE})
	
	#IF the requested date is not in table add it as a row
	if not doesExist({'tableName':tableName, 'columnName':'Date', 'value':date})[ANS]:
		queryString = "INSERT INTO `"+tableName+"` (`Date`) values ('"+date+"')"
		cursor.execute(queryString)
		conn.commit()

	if tableName == NSETABLE:
		valueRes = fetchQuoteFromInternet({'symbol':symbol,'date':date})
		value = valueRes[ANS]
	if tableName == NSETRENDTABLE:
		valueRes = calculateTrend({'symbol':symbol,'date':date})
		value = valueRes[ANS]
	if tableName == NSE1DAYTABLE:
		valueRes = calculateNDayChange({'symbol':symbol, 'date':date, 'N':1})
		value = valueRes[ANS]
	if tableName == NSE3DAYTABLE:
		valueRes = calculateNDayChange({'symbol':symbol, 'date':date, 'N':3})
		value = valueRes[ANS]
	if tableName == NSE5DAYTABLE:
		valueRes = calculateNDayChange({'symbol':symbol, 'date':date, 'N':5})
		value = valueRes[ANS]
	if tableName == NSEFORECASTTABLE:
		valueRes = calculateTrendForecast({'symbol':symbol, 'date':date})
		value = valueRes[ANS]
	if tableName == NSEGOTABLE:
		valueRes = calculateGoNoGo({'symbol':symbol, 'date':date})
		value = valueRes[ANS]
	
	#IF value is zero simply return this to save a unwanted update query
	if value == 0.0:
		res[STATUS] = FAILED
		res[SUBRES] = valueRes
		res[MESSAGE] = "Unable to calculate/fetch the required value regarding "+tableName
		return res
	
	queryString = "update "+tableName+" set `"+symbol+"`='"+`value`+"' where date='"+date+"'"
	try:
		cursor.execute(queryString)
		conn.commit()
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		res[QUERYSTRING]=queryString
		log(ERROR, res)
		return res	
	
	res[ANS] = value
	res[STATUS] = SUCCESS		
	return res	

def getValueFromSymbolDateTable(req):
	#This generic method tries to serve the purpose of table types NSEQuote, NSETrend, NSE5DayChange etc
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	symbol = req['symbol']
	date = req['date']
	tableName = req['tableName']
	queryString="select `"+symbol+"` from "+tableName+" where date='"+date+"'"
	try:
		conn.query(queryString)
		res[ANS]=conn.use_result().fetch_row()[0][0]
		res[STATUS]=SUCCESS
	except Exception as e:
		#res[EXCEPTION]=e
		res[STATUS]=FAILED
		res[QUERYSTRING]=queryString
	if res[ANS] == 0.0:
		res[SUBRES] = setValueFromSymbolDateTable({'tableName':tableName, 'symbol':symbol, 'date':date})
		res[ANS] = res[SUBRES][ANS]
		res[STATUS]=res[SUBRES][STATUS]
	return res

def getQuote(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSETABLE, 'symbol':symbol, 'date':date})

def getTrend(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSETRENDTABLE, 'symbol':symbol, 'date':date})

def get1DayChange(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSE1DAYTABLE, 'symbol':symbol, 'date':date})

def get3DayChange(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSE3DAYTABLE, 'symbol':symbol, 'date':date})

def get5DayChange(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSE5DAYTABLE, 'symbol':symbol, 'date':date})

def getForecast(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSEFORECASTTABLE, 'symbol':symbol, 'date':date})

def getGo(req):
	res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
	date = req['date']
	symbol = req['symbol']
	return getValueFromSymbolDateTable({'tableName':NSEGOTABLE, 'symbol':symbol, 'date':date})

def init():
	log(CRITICAL,"*******************************************************************************")
	log(CRITICAL,"Program starts on :"+today())
	log(CRITICAL,"*******************************************************************************")
	
def executeBTST(req):
    res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
    prices = req['prices']
    #Day 0
    quantity = int(100000 / prices[0])
    #print "Bought "+`quantity`+" shares at "+`day0Price` + " at day"
    boughtValue = 100000
    
    for i in range(1,6):
        price = prices[i]
        marketValue = quantity * price
        percentageChange = getChangePercentage({'old':boughtValue,'new':marketValue})[ANS]
        if percentageChange > 4 or i == 5: #Square Off
            soldValue = marketValue
            #print "Sold "+`quantity`+" shares at "+`price` + "at day" +`i`
            #print "Squaring Off : Bought at "+`boughtValue`+" and Sold at "+`soldValue` + "and made profit/loss of "+`soldValue-boughtValue`
            res[ANS]=soldValue-boughtValue
            res[BOUGHT] = boughtValue
            res[SOLD] = soldValue            
            res[STATUS]=SUCCESS
            return res
        
        toBuyOrSell = int(int(percentageChange)*10000/price)
        
        if toBuyOrSell < 0:
            #print "Bought "+`-toBuyOrSell`+" shares at "+`price` + "at day" +`i`
            quantity = quantity -toBuyOrSell
            boughtValue = boughtValue - toBuyOrSell*price
            """"""
        else:
            #print "Sold "+`toBuyOrSell`+" shares at "+`price` + "at day" +`i`
            quantity = quantity - toBuyOrSell
            boughtValue = boughtValue - toBuyOrSell*price

def executeShortTerm(req):
    res = {METHOD:inspect.stack()[0][3], REQ:req, STATUS:UNKNOWN, ANS:0.0}
    date = req['date']
    symbol = req['symbol']
    days = req['days']
    #Day 0
    quantity = int(100000 / getQuote({'symbol':symbol,'date':date})[ANS])
    #print "Bought "+`quantity`+" shares at "+`day0Price` + " at day"
    boughtValue = 100000
    
    for i in range(1,days+1):
        date = date=getNextNSEDay(date)
        price = getQuote({'symbol':symbol,'date':date})[ANS]
        marketValue = quantity * price
        percentageChange = getChangePercentage({'old':boughtValue,'new':marketValue})[ANS]
        if percentageChange > 4 or i == days: #Square Off
            soldValue = marketValue
            #print "Sold "+`quantity`+" shares at "+`price` + "at day" +`i`
            #print "Squaring Off : Bought at "+`boughtValue`+" and Sold at "+`soldValue` + "and made profit/loss of "+`soldValue-boughtValue`
            res[ANS]=soldValue-boughtValue
            res[BOUGHT] = boughtValue
            res[SOLD] = soldValue            
            res[STATUS]=SUCCESS
            return res
        
        toBuyOrSell = int(int(percentageChange)*10000/price)
        
        if toBuyOrSell < 0:
            #print "Bought "+`-toBuyOrSell`+" shares at "+`price` + "at day" +`i`
            quantity = quantity -toBuyOrSell
            boughtValue = boughtValue - toBuyOrSell*price
            """"""
        else:
            #print "Sold "+`toBuyOrSell`+" shares at "+`price` + "at day" +`i`
            quantity = quantity - toBuyOrSell
            boughtValue = boughtValue - toBuyOrSell*price

#print getGo({'symbol':'WIPRO','date':'2013-10-05'})
#print getPreviousNSEDays('2013-09-24', 5)
#pprint.pprint(getQuote({'symbol':'J&KBANK','date':'2013-10-02'}))
#pprint.pprint(setValueFromSymbolDateTable({'tableName':'NSET', 'symbol':'BANKNIFTY','date':'2013-10-03'}))
#pprint.pprint(fetchQuoteFromInternet({'symbol':'BANKNIFTY','date':'201-10-09'}))
#pprint.pprint(getQuote({'symbol':'WIPRO','date':'2013-10-05'}))
#print getGoogleQuote({'symbol':'WIPRO','date':'2013-10-07'})
#print getGoogleRealTimeQuote({'symbol':'WIPRO'})
#print doesExist({'tableName':'NSE','columnName':'date','value':'2013-03-01'})
#pprint.pprint( getColumnsList({'tableName':NSETABLE}))

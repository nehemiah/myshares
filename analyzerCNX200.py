from SharesLib import *
init()
andersList = ['J&KBANK','L&TFH','M&MFIN','M&M']
toTrend=['ABB','ACC','ADANIENT','ADANIPORTS','ADANIPOWER','ABIRLANUVO','ALBK','AMARAJABAT','AMBUJACEM','AMTEKAUTO','ANDHRABANK','APOLLOHOSP','APOLLOTYRE','ASHOKLEY','ASIANPAINT','AUROPHARMA','AXISBANK','BAJAJ-AUTO','BAJFINANCE','BAJAJFINSV','BAJAJHLDNG','BANKBARODA','BANKINDIA','BATAINDIA','BEL','BHARATFORG','BHEL','BPCL','BHARTIARTL','BHUSANSTL','BIOCON','BOSCHLTD','BRITANNIA','CESC','COREEDUTEC','CADILAHC','CAIRN','CANBK','CASTROL','CENTRALBK','CENTURYTEX','CHAMBLFERT','CIPLA','CUB','COALINDIA','COLPAL','CONCOR','CROMPGREAV','CUMMINSIND','DLF','DABUR','DENABANK','DHFL','DIVISLAB','DRREDDY','EICHERMOT','EMAMILTD','EXIDEIND','FEDERALBNK','FINANTECH','GAIL','GMRINFRA','GVKPIL','GITANJALI','GSKCONS','GLAXO','GLENMARK','GODREJCP','GODREJIND','GRASIM','GUJFLUORO','GUJRATGAS','GMDCLTD','GSPL','HCLTECH','HDFCBANK','HATHWAY','HAVELLS','HEROMOTOCO','HEXAWARE','HINDALCO','HINDPETRO','HINDUNILVR','HDFC','HDIL','ITC','ICICIBANK','IDBI','IDFC','IFCI','INGVYSYABK','IRB','IVRCLINFRA','IDEA','INDIACEM','INDIAINFO','IBREALEST','INDIANB','INDHOTEL','IOB','IGL','INDUSINDBK','INFY','IPCALAB','JSWENERGY','JSWSTEEL','JISLJALEQS','JPASSOCIAT','JPPOWER','JINDALSAW','JINDALSTEL','JUBLFOOD','KTKBANK','KARURVYSYA','KOTAKBANK','LICHSGFIN','LITL','LT','LUPIN','MRF','MADRASCEM','MARICO','MARUTI','MAX','MCLEODRUSS','MINDTREE','MOTHERSUMI','MPHASIS','NHPC','NMDC','NTPC','ONGC','OIL','OPTOCIRCUI','OFSS','ORIENTBANK','ORISSAMINE','PTC','PETRONET','PIDILITIND','PIPAVAVDOC','PEL','PFC','POWERGRID','PUNJLLOYD','PNB','RALLIS','RANBAXY','RAYMOND','RELCAPITAL','RCOM','RELIANCE','RELINFRA','RPOWER','RECLTD','SESAGOA','SHREECEM','RENUKA','SHRIRAMCIT','SRTRANSFIN','SIEMENS','SINTEX','SOBHA','SOUTHBANK','SBIN','SAIL','STAR','SUNPHARMA','SUNTV','SUZLON','SYNDIBANK','TV18BRDCST','TATACHEM','TATACOMM','TCS','TATAGLOBAL','TATAMOTORS','TATAPOWER','TATASTEEL','TECHM','TITAN','TORNTPOWER','UCOBANK','ULTRACEMCO','UNIONBANK','UNITECH','UBL','UNIPHOS','MCDOWELL-N','VIDEOIND','VIJAYABANK','VOLTAS','WELCORP','WOCKPHARMA','YESBANK','ZEEL']
DATE=getPreviousNSEDay(today())
Heading1=DATE
Heading2=DATE

for x in toTrend:
	Heading1+="|"+x+"|||||"
	Heading2+="|Quote|goNoGO|1 Per|3 Per|5 Per|Trend"
        log(DEBUG, "Fore Looping x="+`x`)
print Heading1
print Heading2

date=DATE
for x in range(0,5):
	row=date
	for y in toTrend:
		log(DEBUG, "Looping x="+`x`+" for y="+`y`)
                goNoGo = ""
		forecast = getForecast({'symbol':y,'date':getNextNSEDay(date)})[ANS]
		trend = getTrend({'symbol':y,'date':date})[ANS]
		trend2 = getTrend({'symbol':y,'date':getPreviousNSEDay(date)})[ANS]
		diff = getChangePercentage({'new':forecast, 'old':trend})[ANS]
		diff2 = getChangePercentage({'new':trend, 'old':trend2})[ANS]
		if forecast > 10 and diff > 25 and trend > 0 and diff2 > 25:
			goNoGo = "Go"
		quote=`getQuote({'symbol':y,'date':date})[ANS]`
		oneDayChange=`get1DayChange({'symbol':y,'date':date})[ANS]`
		threeDayChange=`get3DayChange({'symbol':y,'date':date})[ANS]`
		fiveDayChange=`get5DayChange({'symbol':y,'date':date})[ANS]`
		trend=`getTrend({'symbol':y,'date':date})[ANS]`
		row+="|"+quote+"|"+goNoGo+"|"+oneDayChange+"|"+threeDayChange+"|"+fiveDayChange+"|"+trend
	print row
	date=getPreviousNSEDay(date)
	
if isToday(DATE) and isMarketHour("NSE"):
	flush(today())